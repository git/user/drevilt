# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

inherit cmake-utils

DESCRIPTION="The ${PN} is a library that provides application layer protocol support using modern C++ techniques."
HOMEPAGE="http://cpp-netlib.github.com/"
SRC_URI="https://github.com/downloads/${PN}/${PN}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="examples test"

DEPEND="dev-libs/boost"
RDEPEND="${DEPEND}"

src_unpack() {
	unpack ${A}
	cd "${S}"
	epatch "${FILESDIR}"/0001-cmake-fixes.patch
}

src_configure() {
	local mycmakeargs=(
		-DBUILD_SHARED_LIBS=ON
	)
	cmake-utils_src_configure
}

src_install() {
	# emake DESTDIR="${D}" install || die "install failed"
	mkdir -p "${D}"/usr/include/boost
	cp -r boost/* "${D}"/usr/include/boost/
	mkdir -p "${D}"/usr/lib64
	cp ${CMAKE_BUILD_DIR}/libs/network/src/*.so "${D}"/usr/lib64/
}
