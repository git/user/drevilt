# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="3"

inherit eutils

DESCRIPTION="C++ toolkit for extending the Google v8 JavaScript engine"
HOMEPAGE="http://code.google.com/p/v8-juice/"
SRC_URI="http://v8-juice.googlecode.com/files/${P}.tar.bz2"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-lang/v8"
RDEPEND="${DEPEND}"

src_configure() {
	econf
}

src_prepare() {
	mkdir doc/doxygen
	echo -e "install:\n\ttrue" > doc/doxygen/Makefile #  little work around
}

src_install() {
	emake DESTDIR="${D}" install || die "install failed"
}
