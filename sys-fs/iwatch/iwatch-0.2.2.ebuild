# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"


DESCRIPTION="A realtime filesystem monitoring program."
HOMEPAGE="http://sourceforge.net/projects/iwatch"
SRC_URI="mirror://sourceforge/${PN}/${P}.tgz"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="dev-lang/perl
	dev-perl/Linux-Inotify2
	dev-perl/Event
	dev-perl/Mail-Sendmail
	dev-perl/XML-Simple
	dev-perl/XML-SimpleObject"

S="${WORKDIR}/${PN}"

src_prepare() {
	sed -i \
		-e "s|/etc|${ROOT}usr/share/iwatch|" \
		iwatch.xml{,.example} || die "sed failed"
}

src_install() {
	dobin iwatch
	dodoc AUTHORS ChangeLog README iwatch.xml iwatch.xml.example

	insinto /usr/share/iwatch
	doins iwatch.dtd
}

pkg_postinst() {
	elog "Example configuration files are in ${ROOT}usr/share/doc/${PF}"
}
