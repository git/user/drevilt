# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

USE_RUBY="ruby18 ree18 jruby"

inherit ruby-fakegem

DESCRIPTION="htty is a console application for interacting with web servers."
HOMEPAGE="http://htty.github.com/"
LICENSE="MIT"

KEYWORDS="~amd64 ~x86"
SLOT="0"
IUSE=""

ruby_add_rdepend "dev-ruby/mime-types"
