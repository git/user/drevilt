# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

inherit java-pkg-2 java-ant-2

DESCRIPTION="Parallel Ant allows you to automagically execute your ant build in parallel."
HOMEPAGE="http://blog.codeaholics.org/parallel-ant/"
SRC_URI="https://github.com/codeaholics/parallel-ant/tarball/0.9-beta -> ${P}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

COMMON_DEP="dev-java/ant"

RDEPEND=">=virtual/jre-1.5
        ${COMMON_DEP}"

DEPEND=">=virtual/jdk-1.5
        ${COMMON_DEP}"

#java_prepare() {
#	JAVA_ANT_CLASSPATH_TAGS="javac java" java-ant_rewrite-classpath
#}

EANT_GENTOO_CLASSPATH="ant-core"

src_configure() {
	mv codeaholics-${PN}-0a67d42 ${P}
	cd ${P}
}

src_compile() {
	JAVA_ANT_CLASSPATH_TAGS="javac java" java-ant_rewrite-classpath
	eant || die
}

src_install() {
	java-pkg_dojar build/${P}-beta.jar
	insinto /usr/share/${PN}
#	doins "${FILESDIR}/${PN}-${SLOT}"
}
