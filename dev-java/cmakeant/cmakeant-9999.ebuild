# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

inherit java-pkg-2 java-ant-2 eutils mercurial

EHG_REPO_URI="https://code.google.com/p/cmakeant/"

DESCRIPTION="${PN}"
HOMEPAGE="http://code.google.com/p/cmakeant/"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=">=virtual/jdk-1.5"
RDEPEND=">=virtual/jre-1.5"

S=${WORKDIR}

# EANT_GENTOO_CLASSPATH="commons-cli-1,ant-core"

src_install() {
	java-pkg_newjar ${S}/${PN}/${PN}.jar
	# java-pkg_register-dependency xmlunit-1
}

src_compile() {
	cd ${PN} && java-pkg-2_src_compile
}
