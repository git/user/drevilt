# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

EGIT_REPO_URI="git://github.com/douglascrockford/JSON-java.git"

inherit git-2

DESCRIPTION="A reference implementation of a JSON package in Java."
HOMEPAGE="https://github.com/douglascrockford/JSON-java"
SRC_URI=""

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND=""
DEPEND="test? (
		dev-lang/junit
	)"

# incomplete ...
