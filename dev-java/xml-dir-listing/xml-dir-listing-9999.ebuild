# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

inherit java-pkg-2 java-ant-2 eutils subversion

if [ "${PV}" == "9999" ] ; then
	ESVN_REPO_URI="https://xml-dir-listing.googlecode.com/svn/trunk/"
else
	ESVN_REPO_URI="https://xml-dir-listing.googlecode.com/svn/tags/version_${PV}/"
fi

DESCRIPTION="xml-dir-listing"
HOMEPAGE="http://code.google.com/p/xml-dir-listing/"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=">=virtual/jdk-1.5 dev-java/commons-cli dev-java/log4j dev-java/jakarta-regexp"
RDEPEND=">=virtual/jre-1.5 dev-java/commons-cli dev-java/log4j dev-java/jakarta-regexp"

S=${WORKDIR}

EANT_GENTOO_CLASSPATH="commons-cli-1,ant-core"

src_install() {
	java-pkg_newjar ${S}/lib/${PN}.*.jar  ${P}.jar
	# java-pkg_dolauncher ${PN} --jar ${PN}.jar --main net.matthaynes.xml.dirlist.DirectoryListing
	mkdir -p ${D}/usr/bin
        echo -e '#!/bin/bash\njava -cp $(java-config -p xml-dir-listing,commons-cli-1,jakarta-regexp-1.4,log4j) net.matthaynes.xml.dirlist.DirectoryListing -o /dev/stdout $@' > ${D}/usr/bin/xmls
        chmod +x ${D}/usr/bin/xmls
	java-pkg_register-dependency xmlunit-1
}

src_compile() {
	eant || die
	# java-pkg-2_src_compile
}
