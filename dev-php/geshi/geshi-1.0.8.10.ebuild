# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

inherit eutils

DESCRIPTION="${PN}"
HOMEPAGE="http://qbnz.com/highlighter/"
SRC_URI="mirror://sourceforge/geshi/geshi/GeSHi%20${PV}/GeSHi-${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=">=dev-lang/php-5.2"
DEPEND="${REDPEND}"

S=${WORKDIR}/${PN}

src_install() {
	insinto /usr/share/${PN}
	doins -r ${S}/${PN}/*
	insinto /usr/share
	doins ${S}/${PN}.php
	dobin ${FILESDIR}/geshi
}
