# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: Exp $

EAPI="4"
PYTHON_DEPEND="2"

EGIT_REPO_URI="git://github.com/jessemiller/HamlPy.git"

inherit distutils git-2

DESCRIPTION="${PN}"
HOMEPAGE="https://github.com/jessemiller/HamlPy"
SRC_URI=""

LICENSE="MIT" # ?
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="dev-python/setuptools"
RDEPEND="dev-python/django
	dev-python/pygments"
