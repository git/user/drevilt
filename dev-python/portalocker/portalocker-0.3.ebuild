# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: Exp $

EAPI="3"
SUPPORT_PYTHON_ABIS="1"

# inherit distutils

DESCRIPTION="portalocker"
HOMEPAGE="http://code.activestate.com/recipes/65203/"
SRC_URI="http://code.activestate.com/recipes/65203-portalocker-cross-platform-posixnt-api-for-flock-s/download/1/ -> ${P}.py"

LICENSE=""
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

RDEPEND=""
DEPEND="${RDEPEND}"

src_install() {
	mkdir -p "${D}"/usr/bin
	cp "${DISTDIR}"/"${P}".py "${D}"/usr/bin/"${PN}"
	chmod +x "${D}"/usr/bin/"${PN}"
	sed -i"" -e '1 i #!/usr/bin/env python' ${D}/usr/bin/${PN}
}
