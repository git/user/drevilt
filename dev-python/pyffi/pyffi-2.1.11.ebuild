# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: Exp $

EAPI="4"
SUPPORT_PYTHON_ABIS="1"

inherit distutils

DESCRIPTION="${PN}"
HOMEPAGE="http://pyffi.sourceforge.net/"
SRC_URI="mirror://sourceforge/project/pyffi/pyffi/2.1.11/PyFFI-2.1.11.cefd181.tar.bz2"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=""
DEPEND="dev-python/setuptools"

S="${WORKDIR}/PyFFI-2.1.11"

src_install() {
	distutils_src_install
	rm -r ${D}/usr/bin
}
