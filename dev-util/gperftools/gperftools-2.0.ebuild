# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: Exp $

EAPI="4"
inherit autotools eutils

DESCRIPTION=""
HOMEPAGE="http://gperftools.googlecode.com/"
SRC_URI="http://gperftools.googlecode.com/files/${P}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="sys-libs/libunwind"
RDEPEND="${DEPEND}"
