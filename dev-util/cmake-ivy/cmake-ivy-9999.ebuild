# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: ranger Exp $

EAPI="4"

EHG_REPO_URI="https://bitbucket.org/daniperez/cmake-ivy"

inherit mercurial

DESCRIPTION="${PN}"
HOMEPAGE="https://bitbucket.org/daniperez/cmake-ivy/src"
SRC_URI=""

IUSE=""
LICENSE=""
SLOT="0"
KEYWORDS="~x86 ~amd64"

DEPEND=""
RDEPEND="${DEPEND}"

S=${WORKDIR}

src_install() {
	find ${S}
	insinto /usr/share/cmake-ivy
	doins *.cmake CMakeLists.txt
	dodoc README
}
