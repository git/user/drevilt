# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header:  $

inherit apache-module

DESCRIPTION="mod_sass compiles Sass stylesheets into CSS when the sass stylesheet is requested by a clients browser."
HOMEPAGE="http://sourceforge.net/projects/modsass/"

SRC_URI="mirror://sourceforge/${PN/_/}/${PN}-${PV/_/-}.tar.gz"

KEYWORDS="~amd64 ~ppc ~x86"
LICENSE="Apache-2.0"
SLOT="0"
IUSE=""

DEPEND=""
RDEPEND="$DEPEND
	dev-ruby/sass"

APACHE2_MOD_CONF="20_${PN}"
APACHE2_MOD_DEFINE="SASS"

need_apache2_2

S="${WORKDIR}"
